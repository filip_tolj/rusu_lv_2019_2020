Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.

Bilo je potrebno izmjeniti 

for word in words:
        if word not in counts:
            counts[word] = 1
        else:
            counts[word] = 1


u

for word in words:
        if word not in counts:
            counts[word] = 1
        else:
            counts[word] += 1

kako bi se riječi prebrojavale. Prvotni kod je svaku riječ dodao u dictionary i pridodao joj 1 ponavljanje, ali svako iduće ponavljanje se broj ponavljanja nije povećavao. Izmjenom koda svako iduće pojavljivanje iste riječi poveća value vrijednost za 1.
