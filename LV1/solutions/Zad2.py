while True: 
    try:
        number = float(input("Unesite broj: ")) 
        break         
    except ValueError: 
        print ("Niste unjeli broj, pokusajte ponovo!")

if number >= 0.9 :
    print ("Uneseni broj %.2f pripada kategoriji A" % (number))
elif number >= 0.8 and number < 0.9 :
    print ("Uneseni broj %.2f pripada kategoriji B" % (number))
elif number >= 0.7 and number < 0.8 :
    print ("Uneseni broj %.2f pripada kategoriji C" % (number))
elif number >= 0.6 and number < 0.7 :
    print ("Uneseni broj %.2f pripada kategoriji D" % (number))
elif number < 0.6 :
    print ("Uneseni broj %.2f pripada kategoriji F" % (number))

#gotov